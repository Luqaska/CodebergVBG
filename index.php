<?php
include("config.php");
if($https==true){$protocol="https://";}else{$protocol="http://";}
$thisurl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
if(isset($_GET['api'])) {
  include "includes/badges/codeberg.php";
} else {
  header('Content-Type: text/html');
  include "includes/header.php"; 
  if(isset($_GET["generator"])){
    if(isset($_POST["made_generator"])){ 
      $background=str_replace("#","",$_POST["background-color"]); ?>
      <div>
        <form method="POST" class="form-container">
          <label for="claim">Claim: </label>
          <input type="text" name="claim" id="claim" value="<?php echo $_POST['claim'] ?>"><br>
          <label for="badge-type">Type: </label>
          <select name="badge-type">
          <?php if($_POST["badge-type"]=="blue-on-white"){
            $backselect_a=" selected";$backselect_b="";$backselect_c="";
          }elseif($_POST["badge-type"]=="white-on-black"){
            $backselect_a="";$backselect_b=" selected";$backselect_c="";
          }elseif($_POST["badge-type"]=="legacy"){
            $backselect_a="";$backselect_b="";$backselect_c=" selected";
          } ?>
            <option value="blue-on-white"<?php echo $backselect_a ?>>Blue on White</option>
            <option value="white-on-black"<?php echo $backselect_b ?>>White on Black</option>
            <option value="legacy"<?php echo $backselect_c ?>>Legacy</option>
          </select><br>
          <!--<label for="background-color">Background: </label><input type="color" name="background-color" value="<?php echo $_POST['background-color'] ?>"><br>-->
          <button name="made_generator" class="update-btn btn btn-primary" id="button">Update</button>
        </form><br>
        <?php echo "<object id='svg-object' data='$thisurl?api&subtype=".$_POST["badge-type"]."&text=".$_POST["claim"]."' type='image/svg+xml' width='564' height='168'></object><br><code>$thisurl?api&subtype=".$_POST["badge-type"]."&text=".$_POST["claim"]."</code>"; ?>
      </div>
    <?php }else{ ?>
      <div>
        <form method="POST" class="form-container">
          <label for="claim">Claim: </label>
          <input type="text" name="claim" id="claim" value="GET IT ON"/><br>
          <label for="badge-type">Type: </label>
          <select name="badge-type">
            <option value="blue-on-white" selected>Blue on White</option>
            <option value="white-on-black">White on Black</option>
            <option value="legacy">Legacy</option>
          </select><br>
          <!--<label for="background-color">Background: </label><input type="color" name="background-color" value="#ffffff"><br>-->
          <button name="made_generator" class="update-btn btn btn-primary" id="button">Update</button>
        </form><br>
        <object id="svg-object" data="?api&subtype=blue-on-white&text=GET IT ON" type="image/svg+xml" width="564" height="168"></object><br><code><?php echo $thisurl; ?>?api&subtype=blue-on-white&text=GET IT ON</code>
      </div>
    <?php }}else{ ?>
      <h4><span class="badge bg-secondary">GET</span> Generator</h4>
      <!-- <img src="https://getiton.l64.repl.co/api.php?product=codeberg&type=blue-on-white&text=GET+IT" alt="GetItOnCodeberg" width="150px" height="auto"> -->
        <br>
        <ul>
          <code><?php echo $thisurl; ?>?api<a style="color:grey">&subtype=blue-on-white</a><!--<a style="color:orange">&background_color=000000</a>--><a style="color:green">&text=ONLY ON</a></code>
        </ul>
        <br>
        <li style="color:grey">Later the type (blue-on-white/legacy/white-on-black)</li>
        <!--<li style="color: orange;">Optional | Now, it's time for the background color: you can put it in hexdecimal (the code must go without the #, for example "#000000" => "000000"), in letters (for example: "black") or in RGB (rgb(number, number, number), for example rgb(230, 230, 230))</li>-->
        <li style="color:green">To finish: simply, put a text</li>
        <br><br>
        <br>
      </div>
  <?php } ?>
      <br>
<?php include "includes/footer.php";
} ?>