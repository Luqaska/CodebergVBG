<?php if(isset($_GET["generator"])){$page="gen";}else{$page="home";} ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta chardset="utf-8">
    <meta content="text/html" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Lucas' APIs of awesome is a collection of APIs created by me, absucc, in PHP">
    <meta name="keywords" content="API,APIs,SVG,Badges">
    <title>Codeberg VBG</title>
    <link rel="icon" href="https://codeberg.org/img/favicon.svg">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <header>
      <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="https://l64.gitlab.io">
            <img style="text-align: center;" src="https://codeberg.org/img/favicon.svg" width="30" height="30" alt="Codeberg Logo" loading="lazy">
            Vector Badge Generator
          </a>
        </div>
      </nav>
      <p class="menu"><?php if($page=="home"){echo "<b>";} ?><a href="?">Documentation</a><?php if($page=="home"){echo "</b>";} ?> - <?php if($page=="gen"){echo "<b>";} ?><a href="?generator">Generator</a><?php if($page=="gen"){echo "</b>";} ?></p>
    </header>
    <main>