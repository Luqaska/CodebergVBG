# Codeberg Vector Badge Generator
## Features
- Create badges via URL
- Badge generator (select type of badge and the text you want to put to it)
## Requeriments
- PHP web server
## Instalation
1. Git clone or download this branch
2. Put the files on the PHP server
## Want a live version of this?
Visit https://cbapi.l64.repl.co
## Coming soon
- Change the background color & the text color
